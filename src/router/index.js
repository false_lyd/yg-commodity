import Vue from 'vue'
import Router from 'vue-router'
import redWineContent from '../views/content/redwine'
import frozenContent from '../views/content/forzen'
import home from '../views/home/home'
import redwinehome from '../views/home/redwine'
import frozenhome from '../views/home/forzen'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/redwine/home',
      name: 'redwinehome',
      component: redwinehome,
      meta: {
        title: '葡萄酒专区'
      }
    },
    {
      path: '/redwine/:id',
      name: 'redWineContent',
      component: redWineContent,
      meta: {
        title: '葡萄酒专区商品'
      }
    },
    {
      path: '/frozen/home',
      name: 'frozemHome',
      component: frozenhome,
      meta: {
        title: '冻品专区商品列表'
      }
    },
    {
      path: '/frozen/:id',
      name: 'frozenContent',
      component: frozenContent,
      meta: {
        title: '冻品专区'
      }
    }
  ]
})
