import axios from 'axios'

let host = 'http://47.99.185.254:8000'

export const getRedWine = params => {
  if ('id' in params) {
    return axios.get(`${host}/commodity/api/redwine`, {params: params})
  } else {
    return axios.get(`${host}/commodity/api/redwine`)
  }
}

export const getwinery = params => {
  return axios.get(`${host}/commodity/api/winery`, {params: params})
}

export const getfrozen = params => {
  if ('id' in params) {
    return axios.get(`${host}/commodity/api/frozen`, {params: params})
  } else {
    return axios.get(`${host}/commodity/api/frozen`)
  }
}
